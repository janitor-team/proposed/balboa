Source: balboa
Section: net
Priority: optional
Maintainer: Sascha Steinbiss <satta@debian.org>
Build-Depends: debhelper (>= 12),
 dh-golang,
 golang-go (>= 2:1.9),
 golang-github-machinebox-graphql-dev,
 golang-github-graph-gophers-graphql-go-dev,
 golang-github-satori-go.uuid-dev,
 golang-github-spf13-cobra-dev,
 golang-github-spf13-viper-dev,
 golang-github-go-sql-driver-mysql-dev,
 golang-github-sirupsen-logrus-dev (>= 1.0.1),
 golang-github-neowaylabs-wabbit-dev,
 golang-github-gocql-gocql-dev,
 golang-github-data-dog-go-sqlmock-dev,
 golang-github-ugorji-go-codec-dev (>= 1.1.1),
 librocksdb-dev (>= 5.15),
 help2man
Standards-Version: 4.4.0
XS-Go-Import-Path: github.com/DCSO/balboa
Vcs-Browser: https://salsa.debian.org/debian/balboa
Vcs-Git: https://salsa.debian.org/debian/balboa.git
Homepage: https://github.com/DCSO/balboa

Package: balboa
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, adduser
Built-Using: ${misc:Built-Using}
Recommends: balboa-backend-rocksdb
Description: Passive DNS database with GraphQL interface, frontend
 balboa is the BAsic Little Book Of Answers. It consumes and indexes
 observations from passive DNS collection, providing a GraphQL interface
 to access the aggregated contents of the observations database.
 .
 This package contains the frontend, which handles user query interaction
 and input parsing. It requires a backend to store the consumed observations.

Package: balboa-backend-common
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Recommends: balboa
Description: Common files for balboa backends
 balboa is the BAsic Little Book Of Answers. It consumes and indexes
 observations from passive DNS collection, providing a GraphQL interface
 to access the aggregated contents of the observations database.
 .
 This package contains files common to all backends available in Debian,
 such as systemd unit files and management tools.

Package: balboa-backend-rocksdb
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, balboa-backend-common
Description: RocksDB backend for balboa
 balboa is the BAsic Little Book Of Answers. It consumes and indexes
 observations from passive DNS collection, providing a GraphQL interface
 to access the aggregated contents of the observations database.
 .
 This package contains the RocksDB backend.
